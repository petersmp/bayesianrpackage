\name{BernBetaJagsFull}
\alias{BernBetaJagsFull}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Conduct a simple Bernouli test in JAGS
}
\description{
Works with JAGS to run a Bayesian test on a simple, one parameter,
Bernouli process.
}
\usage{
BernBetaJagsFull(model = c(1,1),
                 modelSaveFile = ".tmpModel.txt",
                 theData = factor(rep(c("H", "T"), c(5, 5))),
                 nameSuccess = NULL,
                 parameters = c("theta"),
                 adaptSteps = 500,
                 burnInSteps = 1000,
                 nChains = 3,
                 numSavedSteps = 50000, 
                 thinSteps = 1,
                 nIter = ceiling((numSavedSteps * thinSteps)/nChains), 
                 plot = TRUE,
                 predictValues = FALSE,
                 ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{model}{
  Either a numeric vector of length two, specifying a beta prior, or
  a character vector of length one specifying the model to be used, in JAGS format.
  If set to NULL, the progam will read a file from "modelSaveFile" and use that directly.
}
\item{modelSaveFile}{
  Character sting giving the file name where the model should be saved,
  or naming the file to be read, if \code{model = NULL}.
}
  \item{theData}{
  A vector of data for updating the posterior.
  Can be numeric of 1's and 0's (for success and failure),
  a factor, with the first level (or the level named in \code{nameSuccess})
  representing success, or
  a character vector with the level named in \code{nameSuccess})
  representing success. 
}
  \item{nameSuccess}{
  The value of a \code{theData} to represent a success;
  if \code{NULL} and \code{theData} is a factor,
  the first level is assumed to be a success.
}
  \item{parameters}{
  Character vector of the parameters, from \code{modelString},
  that should be saved.
}
  \item{adaptSteps}{
  Number of steps to "tune" the samplers
}
  \item{burnInSteps}{
  Number of steps to "burn in" the samplers
}
  \item{nChains}{
  Number of chains to run.
}
  \item{numSavedSteps}{
  Total number of steps in chains to save.
}
  \item{thinSteps}{
  Number of steps to "thin" (1=keep every step).
}
  \item{nIter}{
  Number of steps per chain
}
  \item{plot}{
  Logical, should a plot be drawn, or should the values just be returned?
}
  \item{predictValues}{
  Logical, should predicted values be generated.
  Note, this can be a long process.
}
  \item{\dots}{
  Additional parameters passed to \code{\link{plotPost}}.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Invisibly returns a list with
\item{thetaSample}{A vector of length \code{numSavedSteps}
    (as modified by \code{thinSteps})
    giving the generated values of theta from jags}
\item{mcmcInfo}{A data.frame with the summary information as provided by \code{\link{plotPost}},
    for the sampled theta values}
\item{modelInfo}{A list with the model specifications, including
    \code{model} with the specified model, and \code{data} with the data specified in
    \code{theData}}
\item{PredictedValues}{A named numeric vector with the mean Theta and
    mean value from simulated flips.}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Mark Peterson, based on code from John Kruschke.
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{

## Accept all defaults
test <- BernBetaJagsFull()
test$mcmcInfo

## Specify data
test <- BernBetaJagsFull(theData = c(1,1,1,1,1,1,0))
test$mcmcInfo


## Specify different prior
test <- BernBetaJagsFull(model = c(4,9))
test$mcmcInfo


## Make your own model
## Does not have to be beta, but I don't know any better
myModel <- 
"
        # JAGS model specification begins ...
        model {
          # Likelihood:
          for ( i in 1:nFlips ) {
          y[i] ~ dbern( theta )
          }
          # Prior distribution:
          theta ~ dbeta( priorA , priorB )
          priorA <- 10
          priorB <- 20
          }
        # ... JAGS model specification ends.
          "

test <- BernBetaJagsFull(model = myModel)
test$mcmcInfo


}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
