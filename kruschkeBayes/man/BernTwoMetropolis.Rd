\name{BernTwoMetropolis}
\alias{BernTwoMetropolis}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
A home grown metropolis function for two Bernouli processes
}
\description{
Estimates the Bayesian posterior of two Bernouli processes 
using a Metropolis Algorithm MCMC
given a prior and some data.
}
\usage{
BernTwoMetropolis(
  prior = "Beta",
  priorOptions = NULL ,
  likelihood = "standard",
  targetRelProb = "standard",
  theData1 = factor(rep(c("H","T"),c(5,2))),
  theData2 = factor(rep(c("H","T"),c(2,5))),
  nameSuccess = NULL,
  trajLength = 10000,
  burnIn = 0.1 * trajLength ,
  startingValue = c(0.5,0.5),
  jumpSD1 = 0.2,
  jumpSD2 = jumpSD1,
  credMass = 0.95,
  hdiCol = "black",
  analyzeDiff = TRUE,
  plot = TRUE,
  ...

)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{prior}{
  Either a function for defining the prior,
  accepting a vector of each theta of proposed positions,
  or a named value from \code{c(uniform, Beta, Ripples)},
  to use a predefined function.
}
  \item{priorOptions}{
  Options to be passed into the prior functions if giving a named value,
  or NULL if no options are to be passed.
  See Details for more information.
}
  \item{likelihood}{
  A function defining the prior distribution,
  or a character string matching either "standard" for a
  likelihood of based on theta.  
}
  \item{targetRelProb}{
  Either a function to give the relative target probabilities,
  or the character string "standard" for the unnormalized posterior.
}
  \item{theData1}{
  A vector of data for updating the posterior for \code{theta1}.
  Can be numeric of 1's and 0's (for success and failure),
  a factor, with the first level (or the level named in \code{nameSuccess})
  representing success, or
  a character vector with the level named in \code{nameSuccess})
  representing success. 
}
  \item{theData2}{
  A vector of data for updating the posterior for \code{theta2}.
  Can be numeric of 1's and 0's (for success and failure),
  a factor, with the first level (or the level named in \code{nameSuccess})
  representing success, or
  a character vector with the level named in \code{nameSuccess})
  representing success. 
}
  \item{nameSuccess}{
  The value of a \code{theData} to represent a success;
  if \code{NULL} and \code{theData} is a factor,
  the first level is assumed to be a success.
}
  \item{trajLength}{
  How many steps should be taken in the random walk?
}
  \item{burnIn}{
  How many of the walk values should be discarded as burn-in?
  Can be a function of trajLength, and defaults to 10 percent of trajLength.
}
  \item{startingValue}{
  Numeric of length two, where should the random walk start?
  Requires a position for each theta being estimated
}
  \item{jumpSD1}{
  Numeric of length one, what standard deviation should be used
  to generate step sizes for theta1?
}
  \item{jumpSD2}{
  Numeric of length one, what standard deviation should be used
  to generate step sizes for theta2?
}
  \item{credMass}{
  The probability mass of the HDI,
  how wide do you want the plotted HDI to be.
}
  \item{hdiCol}{
  What color should points within the HDI be plotted?
  Set as different from \code{col} in \dots for them to show up as distinct.
}
  \item{analyzeDiff}{
  Should the difference between the two parameters be calculated?
}
\item{plot}{
  Logical, should a plot be drawn, or should the values just be returned?
}
  \item{\dots}{
  Additional parameters passed to plot and postPlot.
}

}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Invisibly returns a list with:
\item{thetaSample}{A matrix with two columns giving the positions
                    of theta1 and theta2 at each step after the burnin}
\item{mcmcInfo}{a data.frame with simple summary information on the Markov chain}
\item{diffInfo}{A data.frame with the summary information as
                provided by \code{\link{plotPost}},
                for the difference between the sampled theta values}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Mark Peterson, modified heavily from code by John Kruschke.
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
BernTwoMetropolis()
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
