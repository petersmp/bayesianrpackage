\name{jagsInstallHelp}
\alias{jagsInstallHelp}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Return point towards jags installation
}
\description{
Brief guide on installing jags and rjags,
designed to be called from within functionst that require both,
when they are not found.
}
\usage{
jagsInstallHelp()
}
%- maybe also 'usage' for other objects documented here.
\details{
This is really here to avoid having to change directions in multiple files.
}
\value{
Returns NULL, but prints a message to the screen with installation instructions.
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Mark Peterson
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function () 
{
    message("To install jags, see: http://mcmc-jags.sourceforge.net/ for help.\n", 
        "After installing jags, use install.packages('rjags') to install rjags.")
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
