################################
###### Moving to package ####### 
################################

## Based on:
# Kruschke, J. K. (2011). Doing Bayesian Data Analysis:
# A Tutorial with R and BUGS. Academic Press / Elsevier.

BernBetaJagsFull <- function(
  model = c(1,1),
  modelSaveFile = ".tmpModel.txt",
  theData = factor(rep(c("H","T"),c(5,5))),
  nameSuccess = NULL,
  parameters = c( "theta" ),     # The parameter(s) to be monitored.
  adaptSteps = 500,              # Number of steps to "tune" the samplers.
  burnInSteps = 1000,            # Number of steps to "burn-in" the samplers.
  nChains = 3,                   # Number of chains to run.
  numSavedSteps = 50000,           # Total number of steps in chains to save.
  thinSteps = 1,                   # Number of steps to "thin" (1=keep every step).
  nIter = ceiling( ( numSavedSteps * thinSteps ) / nChains ), # Steps per chain.
  plot = TRUE,
  predictValues = FALSE,
  ...
  
){
  ## Check and load rjags
  if( !require(rjags) ){
    jagsInstallHelp()
    stop("rjags is not installed. See above help for additional information on installation.")
  }        

  ## Save data names for the output
  theDatafull <- theData
  
  ## Check data and convert
  if(class(theData) == "numeric"){
    if ( any( theData != 1 & theData != 0 ) ) {
      stop("when numeric theData must be a vector of 1s and 0s.") }
  } else {
    theData <- convertDataToBernoulli(theData,nameSuccess)
  }
  #------------------------------------------------------------------------------
  # THE MODEL.
  if(is.numeric(model) & length(model) == 2){
  
  # Specify the model in JAGS language, but save it as a string in R:
    ## Set a default
    modelString = paste("
        # JAGS model specification begins ...
        model {
          # Likelihood:
          for ( i in 1:nFlips ) {
          y[i] ~ dbern( theta )
          }
          # Prior distribution:
          theta ~ dbeta( priorA , priorB )
          priorA <-",model[1],"
          priorB <-",model[2],"
          }
        # ... JAGS model specification ends.
          ") # close quote to end modelString
            
  } else if (is.character(model) & length(model) == 1){
    modelString <- model
  } else if (is.null(model)){
    message("Using already written file from ",modelSaveFile)
    modelString <- paste(readLines(modelSaveFile),collapse = "\n")
  } else {
    stop("model must be either a numeric of length two specifying a beta prior or ",
         "a character of length one specifying a complete model.")
  }
  
  
  
  # Write the modelString to a file, if not using an already written one
  if(!is.null(model)){
    writeLines(modelString,con = modelSaveFile)  
  }
  
  #------------------------------------------------------------------------------
  # THE DATA.
  
  # Specify the data in R, using a list format compatible with JAGS:
  dataList = list(
    nFlips = length(theData) ,
    y = theData
  )
  
  #------------------------------------------------------------------------------
  # INTIALIZE THE CHAIN.
  
  # Can be done automatically in jags.model() by commenting out inits argument.
  # Otherwise could be established as:
  # initsList = list( theta = sum(dataList$y)/length(dataList$y) )
  
  #------------------------------------------------------------------------------
  # RUN THE CHAINS.
  

  # Create, initialize, and adapt the model:
  jagsModel = jags.model( modelSaveFile , data=dataList , # inits=initsList , 
                          n.chains=nChains , n.adapt=adaptSteps )
  # Burn-in:
  message( "Burning in the MCMC chain...\n" )
  update( jagsModel , n.iter=burnInSteps )
  # The saved MCMC chain:
  message( "Sampling final MCMC chain...\n" )
  codaSamples = coda.samples( jagsModel , variable.names=parameters , 
                              n.iter=nIter , thin=thinSteps )
  # resulting codaSamples object has these indices: 
  #   codaSamples[[ chainIdx ]][ stepIdx , paramIdx ]
  
  
  #------------------------------------------------------------------------------
  # EXAMINE THE RESULTS.
  
  # Convert coda-object codaSamples to matrix object for easier handling.
  # But note that this concatenates the different chains into one long chain.
  # Result is mcmcChain[ stepIdx , paramIdx ]
  thetaSample =  as.matrix( codaSamples )
  
  out <- list()
  out$thetaSample <- thetaSample
  
  
  ## Set default Params
  defaultParams <- list(
    xlim=c(0,1),
    xlab=as.expression(bquote(theta) ),
    main="Posterior"   
  )
  
  ## Read in the use supplied additional args
  inDots <- list(...)
  
  ## Set the value of inDots for anything the use didn't supply
  for(thisParam in names(defaultParams)){
    if(is.null(inDots[[thisParam]])){
      inDots[[thisParam]] <- defaultParams[[thisParam]]
    }
  }
  
  ## Set the value of the data for plotting
  inDots$paramSampleVec = thetaSample
  
  ## Set whether or not the plot should be made
  inDots$plot <- plot
  
  ## Run the plot with parameters set above
  out$mcmcInfo <- do.call("plotPost",inDots)    
  
  ## Add model details to output
  out$modelInfo <- list()
  out$modelInfo$model <- modelString
  out$modelInfo$data <- theDatafull
  
  # Posterior prediction:
  if(predictValues){
    # For each step in the chain, use posterior theta to flip a coin:
    chainLength = length( thetaSample )
    yPred = rep( NULL , chainLength )  # define placeholder for flip results
    for ( stepIdx in 1:chainLength ) {
      pHead = thetaSample[stepIdx]
      yPred[stepIdx] = sample( x=c(0,1), prob=c(1-pHead,pHead), size=1 )
    }
    
    out$PredictedValues <- numeric()
    out$PredictedValues["meanSampledTheta"] <- mean(thetaSample) 
    out$PredictedValues["meanFlipValue"] <- mean(yPred) 
    
  }
  

  
  invisible(out)
  
}
