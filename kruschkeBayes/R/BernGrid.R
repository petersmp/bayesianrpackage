BernGrid = function( Theta ,
                     pTheta ,
                     theData ,
                     nameSuccess = NULL,
                     credMass = .95 ,
                     plot = TRUE,
                     nToPlot=length(Theta),
                     ...) {
# Bayesian updating for Bernoulli likelihood and prior specified on a grid.
# Example : BernGrid( c(.25,.5,.75),c(.3,.4,.3),c("H","H","T"),"H")
# Input arguments:
#  Theta is a vector of theta values, all between 0 and 1.
#  pTheta is a vector of corresponding probability _masses_.
#  Data is a vector of 1's and 0's, where 1 corresponds to a and 0 to b.
#  credib is the probability mass of the credible interval, default is 0.95.
#  nToPlot is the number of grid points to plot; defaults to all of them.
# Output:
#  pThetaGivenData is a vector of posterior probability masses over Theta.
#  Also creates a three-panel graph of prior, likelihood, and posterior 
#  probability masses with credible interval.
# Example of use:
#  # Create vector of theta values.
#  > binwidth = 1/1000
#  > thetagrid = seq( from=binwidth/2 , to=1-binwidth/2 , by=binwidth )
#  # Specify probability mass at each theta value.
#  > relprob = pmin(thetagrid,1-thetagrid) # relative prob at each theta
#  > prior = relprob / sum(relprob) # probability mass at each theta
#  # Specify the data vector.
#  > datavec = c( rep(1,3) , rep(0,1) ) # 3 heads, 1 tail
#  # Call the function.
#  > posterior = BernGrid( Theta=thetagrid , pTheta=prior , Data=datavec )

  if(class(theData) == "numeric"){
    if ( any( theData != 1 & theData != 0 ) ) {
      stop("when numeric theData must be a vector of 1s and 0s.") }
  } else {
    theData <- convertDataToBernoulli(theData,nameSuccess)
  }
  

  # Normalize pTheta
  pTheta <- pTheta / sum(pTheta)
  
  # Create summary values of Data
  z = sum( theData==1 ) # number of 1's in Data
  N = length( theData ) # number of flips in Data
  
  # Compute the likelihood of the Data for each value of Theta.
  pDataGivenTheta = Theta^z * (1-Theta)^(N-z)
  
  # Compute the evidence and the posterior.
  pData = sum( pDataGivenTheta * pTheta )
  pThetaGivenData = pDataGivenTheta * pTheta / pData
  names(pThetaGivenData) <- Theta

  meanThetaGivenData = sum( Theta * pThetaGivenData )
  meanTheta = sum( Theta * pTheta ) # mean of prior, for plotting
  
  if(plot){
    # Plot the results.

    ## Reset parameter on exit
    opar <- par(no.readonly = TRUE)  
    on.exit(par(opar)) 
    
    ## Set parameters
    par( mar=c(3,3,1,0) , mgp=c(2,1,0) , mai=c(0.5,0.5,0.3,0.1) ) # margin settings
    layout( matrix( c( 1,2,3 ) ,nrow=3 ,ncol=1 ,byrow=FALSE ) ) # 3x1 panels
    
    # If the comb has a zillion teeth, it's too many to plot, so plot only a
    # thinned out subset of the teeth.
    nteeth = length(Theta)
    if ( nteeth > nToPlot ) {
      thinIdx = seq( 1, nteeth , round( nteeth / nToPlot ) )
      if ( length(thinIdx) < length(Theta) ) {
        thinIdx = c( thinIdx , nteeth ) # makes sure last tooth is included
      }
    } else { thinIdx = 1:nteeth }
    
    ## Set default Params
    defaultParams <- list(
      type = "p" ,
      pch = "." ,
      cex = 5  ,
      xlim = c(0,1) ,
      ## Commenting out to just let R pick the limits
      # ylim=c(0,1.1*max(pTheta)) , 
      xlab=as.expression(bquote(theta) ),
      ylab=as.expression(bquote(p(theta)) ) ,
      main="Prior"   
    )
    
    ## Read in the use supplied additional args
    inDots <- list(...)
    
    ## Set the value of inDots for anything the use didn't supply
    for(thisParam in names(defaultParams)){
      if(is.null(inDots[[thisParam]])){
        inDots[[thisParam]] <- defaultParams[[thisParam]]
      }
    }
    
    ## Set the value of the data for plotting
    inDots$x <- Theta[thinIdx]
    inDots$y <- pTheta[thinIdx]
    
    ## Run the plot with parameters set above
    do.call("plot",inDots)    
    
    
    ## Set the value of the data for plotting the likelihood: p(Data|Theta)
    #if(is.null(list(...)$ylim)){
    #  inDots$ylim <- c(0,1.1*max(pDataGivenTheta))      
    #}
    inDots$x <- Theta[thinIdx]
    inDots$y <- pDataGivenTheta[thinIdx]
    inDots$main <- "Likelihood"
    
    ## Run the plot with parameters set above
    do.call("plot",inDots)
        
    ## Set the value of the data for plotting the the posterior
    ## Commenting out to just let R pick the limits
    # if(is.null(list(...)$ylim)){
    #   inDots$ylim <- c(0,1.1*max(pThetaGivenData))
    # }
    inDots$x <- Theta[thinIdx]
    inDots$y <- pThetaGivenData[thinIdx]
    inDots$main <- "Posterior"
    inDots$ylab <- as.expression(bquote( "p(" * theta * "|D)" ))
    
    
    ## Run the plot with parameters set above
    do.call("plot",inDots)
        
  }
  
  HDIinfo = HDIofGrid( pThetaGivenData , credMass=credMass )
  
  sortedTheta <- sort(Theta)
  isInHDI <- sortedTheta %in% names(HDIinfo$indices)
  
  hdiTemp <- data.frame(min = NA, max = NA)

  if(all(isInHDI) ){
    ## If all are included
    hdiTemp[1,] <- c(min(Theta),max(Theta))
  } else if(!any(isInHDI) ){
    ## If somehow none are included
    hdiTemp[1,] <- c(NA,NA)
  } else {
    inHDIidx <- which(isInHDI)
    isOneMore <- (inHDIidx[2:length(inHDIidx)] - inHDIidx[1:(length(inHDIidx)-1)] ) == 1
    if(all(isOneMore)){
      ## If these are continuous
      hdiTemp[1,] <- sortedTheta[c(inHDIidx[1],inHDIidx[length(inHDIidx)]) ]
    } else {
      tempMin <- sortedTheta[inHDIidx[c(1, which(!isOneMore)+1  )]]
      tempMax <- sortedTheta[inHDIidx[c(which(!isOneMore),length(inHDIidx))]]
      hdiTemp <- data.frame(min = tempMin,max= tempMax)
    }
  }

## Old method for plotting HDI
#   points( Theta[ HDIinfo$indices ] ,
#           rep( HDIinfo$height , length( HDIinfo$indices ) ) , pch="-" , cex=1.0 )
#   text( mean( Theta[ HDIinfo$indices ] ) , HDIinfo$height ,
#         bquote( .(100*signif(HDIinfo$mass,3)) * "% HDI" ) ,
#         adj=c(0.5,-1.5) , cex=1.5 )
#   # Mark the left and right ends of the waterline. 
#   # Find indices at ends of sub-intervals:
#   inLim = HDIinfo$indices[1] # first point
#   for ( idx in 2:(length(HDIinfo$indices)-1) ) {
#     if ( ( HDIinfo$indices[idx] != HDIinfo$indices[idx-1]+1 ) | # jumps on left, OR
#            ( HDIinfo$indices[idx] != HDIinfo$indices[idx+1]-1 ) ) { # jumps on right
#       inLim = c(inLim,HDIinfo$indices[idx]) # include idx
#     }
#   }
#   inLim = c(inLim,HDIinfo$indices[length(HDIinfo$indices)]) # last point
#   # Mark vertical lines at ends of sub-intervals:
#   for ( idx in inLim ) {
#     lines( c(Theta[idx],Theta[idx]) , c(-0.5,HDIinfo$height) , type="l" , lty=2 , 
#            lwd=1.5 )
#     text( Theta[idx] , HDIinfo$height , bquote(.(round(Theta[idx],3))) ,
#           adj=c(0.5,-0.1) , cex=1.2 )
#   }
#   
  out <- list()
  out$posterior <- pThetaGivenData
  out$meanPosterior <- meanThetaGivenData
  out$HDIranges <- hdiTemp
  out$pData <- pData
  invisible( out )
} # end of function