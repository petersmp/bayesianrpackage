BetaPosteriorPredictions <- function(
  priorShape = c(1,100),
  theData = factor(rep(c("H","T"),times=c(8,4))),
  nameSuccess = NULL,
  nSimSamples = 10000,
  plot = TRUE,
  ...
  ){
  
  # Check for errors in input arguments:
  if ( length(priorShape) != 2 ) {
    stop("priorShape must have two components.") }
  if ( any( priorShape <= 0 ) ) {
    stop("priorShape components must be positive.") }
  
  
  if(class(theData) == "numeric"){
    if ( any( theData != 1 & theData != 0 ) ) {
      stop("when numeric theData must be a vector of 1s and 0s.") }
  } else {
    theData <- convertDataToBernoulli(theData,nameSuccess)
  }
  
  # Rename the prior shape parameters, for convenience:
  priorA = priorShape[1]
  priorB = priorShape[2]
  
  
  ## Count the number of ones, and the length
  actualDataZ <- sum(theData) 
  actualDataN <- length(theData)
  
  # Specify known values of prior and actual data.
  # Compute posterior parameter values.
  postA = priorA + actualDataZ
  postB = priorB + actualDataN - actualDataZ
  
  # Number of flips in a simulated sample should match the actual sample size:
  simSampleSize = actualDataN
  
  # Set aside a vector in which to store the simulation results.
  simSampleZrecord = vector( length=nSimSamples )
  
  # Now generate samples from the posterior.
  for ( sampleIdx in 1:nSimSamples ) {
    # Generate a theta value for the new sample from the posterior.
    sampleTheta = rbeta( 1 , postA , postB )
    # Generate a sample, using sampleTheta.
    sampleData = sample( x=c(0,1) , prob=c( 1-sampleTheta , sampleTheta ) ,
                         size=simSampleSize , replace=TRUE )
    # Store the number of heads in sampleData.
    simSampleZrecord[ sampleIdx ] = sum( sampleData )
  }
  
  if(is.null(nameSuccess)){
    nameSuccess <- "successes"
  }

  out <- table(factor(simSampleZrecord,levels = 0:actualDataN),dnn =  paste("Number of", nameSuccess, "in simulation"))
  
  
  # Make a histogram of the number of heads in the samples.
  if(plot){
    
    ## Set default Params
    defaultParams <- list(
      main = "",
      ylab = "Frequency",
      xlab = paste("Number of", nameSuccess, "in simulation"),
      lwd = 96 / length(out)   
    )
    
    ## Read in the use supplied additional args
    inDots <- list(...)
    
    ## Set the value of inDots for anything the use didn't supply
    for(thisParam in names(defaultParams)){
      if(is.null(inDots[[thisParam]])){
        inDots[[thisParam]] <- defaultParams[[thisParam]]
      }
    }
    
    ## Set the data to plot
    inDots$x <- out
    
    ## Make the plot
    do.call("plot",inDots)
    
    ## An optional way to add a line to the plot for the actual data,
    ##   but, it seems to not work quite the way I want it to.
#     abline(v=actualDataZ,col="red")
#     opar <- par(fig=c(0, 1, 0, 1), oma=c(0, 0, 0, 0), 
#                 mar=c(0, 0, 0, 0), new=TRUE)
#     on.exit(par(opar))
#     plot(0, 0, type='n', bty='n', xaxt='n', yaxt='n')
#     legend("top",xpd = TRUE,bty = "n",
#            legend = paste(actualDataZ,nameSuccess),
#            title =  NULL,
#            lty = 1,
#            inset = c(0,0.02),
#            col="red")

  }
  
  
  return(out)
}
  
