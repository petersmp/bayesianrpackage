---
title: "Chapter 3"
author: "Mark Peterson"
date: "2014/July/01"
output:
  pdf_document:
    toc: false


---

```{r, echo=FALSE} 
setwd("~/Documents/Juniata/teaching/learningBayes/myScripts")
library(knitr)
set.seed(16652)
probTen = 1/6
```



Exercises
=========


Exercise 3.1
------------

I wasn't a fan of his program, so here is a quicker one.
(To self: see notes from reading for example with a die.)

```{r}
# Set coin probability
coinProb <- 0.8

# Flip the coins
flips <- sample(c("H","T"),1000,replace = TRUE,prob = c(coinProb,1-coinProb))

# Calculate running proporition
runningProb <- cumsum(flips == "H")/ cumsum(rep(1,length(flips)))

# Plot the run with a line for the coin probability
plot(1:length(flips),runningProb,
     main = paste("Plot with p(H) =",coinProb),
     ylim=c(0,1),
     xlab="Flip",ylab="Proportion Heads")
abline(h=coinProb,col='red',lwd=3)
```



Exercise 3.2
------------

<a name="part3.1a"></a>Part A:

The probability of drawing a ten is 1 in 6 (`r 1/6`).
Of the 48 cards in the deck, 8 are tens.

$$\frac{8}{48} = \frac{1}{6}$$

Similarly, there are 6 face values,
each represented the same number of times (8).
So, we could infer the $\frac{1}{6}$ from that as well.

Part B:

The probabilty of either of two mutually exclusive events occuring is
the sum of their individual probabilities,
and the probability of any single face value occuring is $\frac{1}{6}$
(see [part A](#part3.1a)).
Thus, the proability of drawing either a ten *or* a jack is given by:

$$\frac{1}{6} + \frac{1}{6} = \frac{1}{3}$$


\clearpage

Exercise 3.3
------------

Part A:

Again, not a fan of the clunky code.
Cleaner code 

```{r}
## Set interval of interest
minX <- 0
maxX <- 1

## Size of steps
dx <- 0.02

## Set x values
x <- seq(from = minX, to = maxX, by = dx)

## Set y values
y <- 6*x * (1 - x)

## From the author:
# Plot the function. "plot" draws the intervals. "lines" draws the bell curve.
plot( x , y , type="h" , lwd=1 
  , xlab="x" , ylab="p(x)" 
	, main="Probability Density" )
lines( x , y )
area = sum( dx * y )
area
```


Part B:

$$ \int_0^1 \! \mathrm{d}x \, p(x) $$
 
$$ \int_0^1 \! \mathrm{d}x \, 6x(1 - x) $$

$$ 6 \int_0^1 \! \mathrm{d}x \, (x - x^2) $$ 

$$ 6 \left[  \frac{1}{2}x^2 - \frac{1}{3}x^3   \right]_0^1 $$

$$  6 \left[  \left( \frac{1}{2}1^2 - \frac{1}{3}1^3 \right) - \left( \frac{1}{2}0^2 - \frac{1}{3}0^3 \right)\right]$$

$$  6 \left( \frac{1}{2} - \frac{1}{3} \right) $$

$$  6 \left( \frac{1}{6} \right) $$


Part C:

Yes, the area sums to 1, which is what is expected and required.
This is also approximately the calcualted value from the intervals.

Part D:

The maximum value of $p(x)$ appears to be 0.5,
the same as the expected value calculated in equation 3.7.




Exercise 3.4
------------

Part A:

Using the code because it is handier for something this close
However, I really dislike the addition of the means to the plots,
so will output it with the R code instead.
```{r}
# Graph of normal probability density function, with comb of intervals.
meanval = 0.0             # Specify mean of distribution.
sdval = 0.2               # Specify standard deviation of distribution.
xlow  = meanval - sdval # Specify low end of x-axis.
xhigh = meanval + sdval # Specify high end of x-axis.
dx = 0.01                 # Specify interval width on x-axis
# Specify comb points along the x axis:
x = seq( from = xlow , to = xhigh , by = dx )
# Compute y values, i.e., probability density at each value of x:
y = ( 1/(sdval*sqrt(2*pi)) ) * exp( -.5 * ((x-meanval)/sdval)^2 )
# Plot the function. "plot" draws the intervals. "lines" draws the bell curve.
plot( x , y , type="h" , lwd=1 , cex.axis=1.5
  , xlab="x" , ylab="p(x)" , cex.lab=1.5
	, main="Normal Probability Density" , cex.main=1.5 )
lines( x , y )
# Approximate the integral as the sum of width * height for each interval.
area = sum( dx * y )
area
```

The area within one standard deviation of the mean with
this interval (`r dx`) is `r area`,
which is just over two thirds of the area.


Part B:


A normal distribution with mean 162 cm and
a standard deviation of 15 cm
captures the belief that about two thirds of women
are between 147 cm and 177 cm,
with mean of 162 cm,
though a standard deviation of 15.5 gets even closer.

```{r}
# Graph of normal probability density function, with comb of intervals.
meanval = 162             # Specify mean of distribution.
sdval = 15.5             # Specify standard deviation of distribution.
xlow  = 147 # Specify low end of x-axis.
xhigh = 177 # Specify high end of x-axis.
dx = 0.01                 # Specify interval width on x-axis
# Specify comb points along the x axis:
x = seq( from = xlow , to = xhigh , by = dx )
# Compute y values, i.e., probability density at each value of x:
y = ( 1/(sdval*sqrt(2*pi)) ) * exp( -.5 * ((x-meanval)/sdval)^2 )

area = sum( dx * y )
area
```

Exercise 3.5
------------


```{r}
grades <- paste(c("1st","6th","11th"),"Grade")
foods <- c("Ice Cream","Fruit","French Fries")
propInGrade <- c( .2,.2,.6)
names(propInGrade) <- grades
propByGrade <- matrix( c(0.3,0.6,0.1,  # 1st 
                       0.6,0.3,0.1,  # 6th
                       0.3,0.1,0.6), # 11th
                     nrow = length(grades),byrow = TRUE,
                     dimnames = list(grades,foods))
```

```{r results="asis", echo=FALSE}
kable(propByGrade, format = "markdown")
```

```{r}
## Construct the conjoint table
## Multiples each column by the vector of prop in grade
conjointProb <- apply(propByGrade,2,function(x) {x*propInGrade})
```

Giving these conjoint probabilites:

```{r results="asis", echo=FALSE}
kable(addmargins(conjointProb), format = "markdown",)
```

If these two things were independent, I would expect the distribution below.
Which looks rather different than our actual conjoint,
suggesting that these distributions are not indpendent.

```{r}
## Calculate the proportion likeing each food
propInFood <- apply(conjointProb,2,sum)

## Calaculte the products
indConjoint <- apply(as.matrix(propInFood), 1, function(x) {x*propInGrade})
```

```{r results="asis", echo=FALSE}
kable(addmargins(indConjoint), format = "markdown",)
```


