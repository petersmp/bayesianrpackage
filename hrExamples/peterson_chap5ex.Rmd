---
title: "Chapter 5"
author: "Mark Peterson"
date: "2014/July/01"
output:
  pdf_document:
    toc: false


---

```{r, echo=FALSE} 
setwd("~/Documents/Juniata/teaching/learningBayes/myScripts")
library(knitr)
set.seed(16652)
opts_chunk$set(fig.width=4, fig.height=4)
options("scipen"=7 ) ##, "digits"=6) ## This was to control table printing
```



Exercises
=========

Exercise 5.1
------------

### Part A

```{r}
## Note, I commented out the source() argument within these
## For functions like this, an R package would be easier
source("../programsFromAuthor/BernBeta.R")
source("../programsFromAuthor/HDIofICDF.R")
post <- BernBeta(priorShape = c(4,4),dataVec = c(1) )
```

The posterior distribution is a beta distribtion of 
`r paste("beta("," $\\theta$ ", "|",post[1],",",post[2],")",sep="") `.

### Part B

```{r fig.show='hide'}
postB <- BernBeta(priorShape = post,dataVec = c(1) )
```

This update gives a posterior distribution is a beta distribtion of 
`r paste("beta("," $\\theta$ ", "|",postB[1],",",postB[2],")",sep="") `.

### Part C

```{r fig.show='hide'}
postC <- BernBeta(priorShape = postB,dataVec = c(0) )
```

This update gives a posterior distribution is a beta distribtion of 
`r paste("beta("," $\\theta$ ", "|",postC[1],",",postC[2],")",sep="") `.


### Part D

```{r fig.show='hide'}
post  <- BernBeta(priorShape = c(4,4) ,dataVec = c(0) )
postB <- BernBeta(priorShape = postB  ,dataVec = c(1) )
postC <- BernBeta(priorShape = postC  ,dataVec = c(1) )
```

This update gives a posterior distribution is a beta distribtion of 
`r paste("beta("," $\\theta$ ", "|",postC[1],",",postC[2],")",sep="") `.
Which is the same as the distribution in part C.
This could be seen to be obviously true on its face by considering that
this is defined to be a property of these approaches,
and that each update is the equivalent of adding an observation
to either "a"" or "b", thus making the order irrelevant.




Exercise 5.2
------------

### Part A

```{r}
prior <- c(1,1)
nTotal <- 100
nPreferA <- 58
theData <- rep(c(1,0),times = c(nPreferA, nTotal-nPreferA))
post <- BernBeta(prior,theData)
```

### Part B

Yes, following the poll, the HDI of our posterior
includes the value of 0.5, which suggest that the value
remains reasonably likely.


```{r}
prior <- post
nTotal <- 100
nPreferA <- 57
theData <- rep(c(1,0),times = c(nPreferA, nTotal-nPreferA))
postB <- BernBeta(prior,theData)
```

The follow up poll yields a 95% HDI that ranges from
`r paste(round(HDIofICDF(qbeta,shape1 = postB[1], shape2 = postB[2]),3), collapse = " to ") `.
This no longer includes an exactly even split.
However, it does include values very close to an even split,
and so a split that is very close to even
(i.e., within a reasonable ROPE)
is still quite likely.


Exercise 5.3
------------

For the scenario when shown the word "radio":

```{r fig.show='hide'}
prior <- c(1,1)
nTotal <- 50
nSelectF <- 40
## The below just makes it a little cleaner, in my opinion
responses <- rep(c("F","J"),times = c(nSelectF, nTotal-nSelectF))
theData <- as.numeric(responses == "F")
post <- BernBeta(prior,theData)
```

It appears that participants are biased towards selecting "F"
when presented with the word "radio"
as the 95% HDI of the posterior does not include 0.5.


For the scenario when shown the words
"mountain" and "ocean":

```{r fig.show='hide'}
prior <- c(1,1)
nTotal <- 50
nSelectF <- 15
## The below just makes it a little cleaner, in my opinion
responses <- rep(c("F","J"),times = c(nSelectF, nTotal-nSelectF))
theData <- as.numeric(responses == "F")
post <- BernBeta(prior,theData)
```

It appears that participants are biased towards selecting "J"
when presented with the words "mountain" and "ocean"
as the 95% HDI of the posterior does not include 0.5.

Given these results, I want to go read the referenced paper.
The first result suggests a primacy effect
(they learned the "F" response first).
The second result suggests either a recency effect
or, if the words are presented in the order shown in the question,
that respondents are assuming that the position of the words make a difference.



Exercise 5.4
============

```{r fig.show='hide'}
prior <- c(0.5,0.5)
nTotal <- 5
nHeads <- 4
## The below just makes it a little cleaner, in my opinion
flips <- rep(c("H","T"),times = c(nHeads, nTotal-nHeads))
theData <- as.numeric(flips == "H")
post <- BernBeta(prior,theData)
```

The posterior distribution is a beta distribtion of 
`r paste("beta("," $\\theta$ ", "|",post[1],",",post[2],")",sep="") `.
which has a 95% HDI ranging from
`r paste(round(HDIofICDF(qbeta,shape1 = post[1], shape2 = post[2]),3), collapse = " to ") `


Exercise 5.5
============

### Part A

For a coin that we presume is likely to be fair,
we may set a prior with a lot of confidence,
such as the below, which is equivalent to
assuming that our prior is similar to if we had seen 30 fair flips already.
Having just started to play with priors,
I would be willing to entertain other values as well.

```{r fig.show='hide'}
prior <- c(15,15)
nTotal <- 10
nHeads <- 9
## The below just makes it a little cleaner, in my opinion
flips <- rep(c("H","T"),times = c(nHeads, nTotal-nHeads))
theData <- as.numeric(flips == "H")
post <- BernBeta(prior,theData)
```

The posterior distribution is a beta distribtion of 
`r paste("beta("," $\\theta$ ", "|",post[1],",",post[2],")",sep="") `.
which has a mean predicted value of `r round(post[1] / sum(post),4)`,
suggesting that we would expect our next flip to be heads
`r round(100 * post[1] / sum(post),2)`% of the time.


### Part B

For a coin that we think is likely to be biased,
we would set a very different prior.
The prior here is the same as in Figure 5.1 at the top left.
It suggests that the most likely values are biased\
to one extreme or the other.
Having just started to play with priors,
I would be willing to entertain other values as well.

```{r fig.show='hide'}
prior <- c(.5,.5)
nTotal <- 10
nHeads <- 9
## The below just makes it a little cleaner, in my opinion
flips <- rep(c("H","T"),times = c(nHeads, nTotal-nHeads))
theData <- as.numeric(flips == "H")
post <- BernBeta(prior,theData)
```

The posterior distribution is a beta distribtion of 
`r paste("beta("," $\\theta$ ", "|",post[1],",",post[2],")",sep="") `.
which has a mean predicted value of `r round(post[1] / sum(post),4)`,
suggesting that we would expect our next flip to be heads
`r round(100 * post[1] / sum(post),2)`% of the time.


Exercise 5.6
============

For this, I will use the same priors as used in Excercise 5.5
(see that for justifications).

```{r fig.show='hide'}
priorFair <- c(15,15)
priorTrick <- c(.5,.5)
nTotal <- 20
nHeads <- 15
## The below just makes it a little cleaner, in my opinion
flips <- rep(c("H","T"),times = c(nHeads, nTotal-nHeads))
theData <- as.numeric(flips == "H")
pDataFair  <- beta( nHeads+priorFair[1] ,
                    nTotal-nHeads+priorFair[2] ) /
              beta( priorFair[1] , priorFair[2])
pDataTrick <- beta( nHeads+priorTrick[1] ,
                    nTotal-nHeads+priorTrick[2] ) /
              beta( priorTrick[1] , priorTrick[2] )

```


From these, we see that for the two models,
the probability of the data is
`r pDataFair`
for the fair coin prior and
`r pDataTrick`
for the trick-coin prior.
This suggests that it is slightly more likely to be a trick coin,
though the bayes factor is only
`r pDataTrick/pDataFair`
meaning that the trick coin is only
`r round( (100*pDataTrick/pDataFair) - 100,2) `%
more likely than the fair,
given these priors.




Exercise 5.7
============


```{r fig.show='hide'}
priorHead <- c(100,1)
priorTail <- c(1,100)
nTotal <- 1
nHeads <- 1
## The below just makes it a little cleaner, in my opinion
flips <- rep(c("H","T"),times = c(nHeads, nTotal-nHeads))
theData <- as.numeric(flips == "H")
pDataHead  <- beta( nHeads+priorHead[1] ,
                    nTotal-nHeads+priorHead[2] ) /
              beta( priorHead[1] , priorHead[2])
pDataTail  <- beta( nHeads+priorTail[1] ,
                    nTotal-nHeads+priorTail[2] ) /
              beta( priorTail[1] , priorTail[2] )

```


From these, we see that for the two models,
the probability of the data is
`r pDataHead`
for the heads-biased prior and
`r pDataTail`
for the tails-biased prior.
This suggests that it is substantially more likely
to be a heads-biased coin than a tails-biased coin
Here, The bayes factor is
`r pDataHead/pDataTail`
meaning that the heads-biased prior is
`r pDataHead/pDataTail` times more likely than the
tails-biased prior.
However, this assumes it must be one or the other,
so the conclusion is only as valid as is our assumption
that the coin must be biased.



Exercise 5.8
============

### Part A

Running the code provided in BetaPosteriorPredictions.R,
with minor modifications to improve the plot returns
this histogram.

```{r echo=FALSE}
# Specify known values of prior and actual data.
priorA = 100
priorB = 1
actualDataZ  = 8
actualDataN  = 12
# Compute posterior parameter values.
postA = priorA + actualDataZ
postB = priorB + actualDataN - actualDataZ
# Number of flips in a simulated sample should match the actual sample size:
simSampleSize = actualDataN
# Designate an arbitrarily large number of simulated samples.
nSimSamples = 10000
# Set aside a vector in which to store the simulation results.
simSampleZrecord = vector( length=nSimSamples )
# Now generate samples from the posterior.
for ( sampleIdx in 1:nSimSamples ) {
  # Generate a theta value for the new sample from the posterior.
	sampleTheta = rbeta( 1 , postA , postB )
	# Generate a sample, using sampleTheta.
	sampleData = sample( x=c(0,1) , prob=c( 1-sampleTheta , sampleTheta ) ,
                          size=simSampleSize , replace=TRUE )
	# Store the number of heads in sampleData.
	simSampleZrecord[ sampleIdx ] = sum( sampleData )
}
# Make a histogram of the number of heads in the samples.
hist( simSampleZrecord,
      breaks = 0:actualDataN,
      main="Histogram of Simulated Results",
      xlab = "Number of heads in simulation")
```

### Part B

The program simulated `r nSimSamples` samples,
each of size `r actualDataN`.

### Part C

No. A new value was choosen for $\theta$ on each simulation
in order to
reflect our uncertainty in the estimate of $\theta$.
This allows the simulation to use values of $\theta$
distributed as predicted by our posterior.

### Part D

No, based on the simulation, our "winning" heads-biased model
is a terrible model for the actual data.
Only `r sum(simSampleZrecord == actualDataZ)` of the 
`r nSimSamples` simulations had `r actualDataZ` heads (like the observed data),
while the most common result of the simulation was
`r names(which.max(table (simSampleZrecord))) ` heads
(observed in `r max(table (simSampleZrecord))`
of the `r nSimSamples` simulations).


It seems that the most likely explanation is that the coin is not actually
biased towards either heads or tails by as much as originally predicted.

